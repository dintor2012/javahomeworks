import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.ashot.AShot;

import java.time.Duration;
import java.util.List;

public class avitoTest {
    private WebDriver driver;
    //private AShot shot;

   /* @Attachment
    public byte[] captureScreenshot(WebDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }*/

    //@Step("Prepare WebDriver")
    @BeforeTest
    public void startingDriver() {
        System.setProperty("webdriver.chrome.driver", "C:/chromedriver/chromedriver.exe");
        driver = new ChromeDriver();

    }

    @Description("Open resource")
    @Test
    public void Test1() {
        driver.get("https://www.avito.ru/");
        //captureScreenshot(driver);
    }

    @Description("Find category")
    @Test
    public void Test2() {
        Select category = new Select(driver.findElement(By.name("category_id")));
        category.selectByValue("99");

    }

    //@Step("Input printer")
    @Description("Find printer")
    @Test
    public void Test3() {
        WebElement search = driver.findElement(By.className("input-input-Zpzc1"));
        search.sendKeys("Принтер");

    }

    //@Step("Select  city")
    @Description("Select City")
    @Test
    public void Test4() {
        WebElement citySearch = driver.findElement(By.className("main-text-_Thor"));
        citySearch.click();

    }

    //@Step("Input city and click")
    @Description("Find city by name and click")
    @Test
    public void Test5() {
        WebElement inputCity = driver.findElement(By.className("suggest-input-rORJM"));
        inputCity.sendKeys("Владивосток");


        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span/strong")));
        WebElement findFirstValue = driver.findElement(By.xpath("//span/strong"));
        findFirstValue.click();
        WebElement acceptButton = driver.findElement(By.xpath("//button[@data-marker='popup-location/save-button']"));
        acceptButton.click();
    }

    //@Step("Check checkbox and show ads")
    @Description("Checkbox and ads")
    @Test
    public void Test6() {
        WebElement checkBox = (new WebDriverWait(driver, Duration.ofMinutes(2)))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@data-marker='delivery-filter/input']")));
        checkBox.sendKeys(Keys.SPACE);

        WebElement confirmButton = driver.findElement(By.xpath("//button[@data-marker='search-filters/submit-button']"));
        if (checkBox.isSelected()) {
            confirmButton.click();
        }
    }

    //@Step("Sort by price")
    @Description("Sort by price")
    @Test
    public void Test7() {
        driver.findElement(By.xpath("//option[contains(text(),'Дороже')]")).click();
    }

    //@Step("Show top 3 printers by price")
    @Description("Top 3 printers output")
    @Test
    public void Test8() {
        List<WebElement> elem = driver.findElements(By.xpath("//div[@data-marker='catalog-serp']/div[position() <= 3]"));
        for (WebElement element : elem) {
            System.out.println(element.findElement(By.xpath(".//h3[@itemprop='name']")).getText());
            System.out.println(element.findElement(By.xpath(".//meta[@itemprop='price']")).getAttribute("content"));

        }
    }

    //@Step("Quit WebDriver")
    @AfterTest
    public void closeDriver() {
        driver.quit();
    }

}



