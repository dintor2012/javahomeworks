import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;


public class Avito {

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:/chromedriver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.avito.ru/");

        Select category = new Select(driver.findElement(By.name("category_id")));
        category.selectByValue("99");

        WebElement search = driver.findElement(By.className("input-input-Zpzc1"));
        search.sendKeys("Принтер");

        WebElement citySearch = driver.findElement(By.className("main-text-_Thor"));
        citySearch.click();

        WebElement inputCity = driver.findElement(By.className("suggest-input-rORJM"));
        inputCity.sendKeys("Владивосток");

        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span/strong")));

        WebElement findFirstValue = driver.findElement(By.xpath("//span/strong"));
        findFirstValue.click();
        WebElement acceptButton = driver.findElement(By.xpath("//button[@data-marker='popup-location/save-button']"));
        acceptButton.click();

        WebElement checkBox = (new WebDriverWait(driver, Duration.ofMinutes(2)))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@data-marker='delivery-filter/input']")));
        checkBox.sendKeys(Keys.SPACE);

        WebElement confirmButton = driver.findElement(By.xpath("//button[@data-marker='search-filters/submit-button']"));
        if (checkBox.isSelected()) {
            confirmButton.click();
        }

        driver.findElement(By.xpath("//option[contains(text(),'Дороже')]")).click();

        List<WebElement> elem = driver.findElements(By.xpath("//div[@data-marker='catalog-serp']/div[position() <= 3]"));
        for (WebElement element : elem) {
            System.out.println(element.findElement(By.xpath(".//h3[@itemprop='name']")).getText());
            System.out.println(element.findElement(By.xpath(".//meta[@itemprop='price']")).getAttribute("content"));


        }

    }
}
