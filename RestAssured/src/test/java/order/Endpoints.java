package order;

public final class Endpoints {
    public static String host = "https://petstore.swagger.io/v2/store";
    public static String order = "/order";
    public static String orderId = "/{id}";
    public static String inventory = "/inventory";
}
