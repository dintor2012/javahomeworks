import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import order.Order;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import order.Endpoints;

import java.util.Map;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.testng.Assert.assertEquals;

public class SwaggerOrderTest {

    @BeforeClass
    static void setUp() {
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri(Endpoints.host)
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
        RestAssured.filters(new ResponseLoggingFilter());
    }

    private Order createOrder() {
        Order order = new Order();
        order.setPetId(new Random().nextInt(10000));
        order.setId(new Random().nextInt(100));
        order.setQuantity(new Random().nextInt(10));
        return order;
    }

    private void postOrder(Order order) {
        given()
                .body(order)
                .when()
                .post(Endpoints.order)
                .then()
                .statusCode(200);
    }

    private Order getOrder(int id) {
        return given().pathParam("id", id)
                .when().get(Endpoints.order + Endpoints.orderId)
                .then().assertThat().
                statusCode(200).extract().body().as(Order.class);
    }

    private void deleteOrder(int id) {
        when().delete(Endpoints.order + "/" + id).then().log().body().assertThat().statusCode(200);
    }

    @Test(priority = 1)
    public void postOrderTest() {
        Order order = createOrder();
        postOrder(order);

    }
    @Test(priority = 2)
    public void getOrderTest(){
        Order order = createOrder();
        postOrder(order);
        Order apiOrder = getOrder(order.getId());
        assertEquals(order, apiOrder);
    }
    @Test(priority = 3)
    public void deleteOrderTest(){
        Order order = createOrder();
        postOrder(order);
        deleteOrder(order.getId());
    }

    @Test(priority = 4)
    void getStoreInventory() {
        Map inventory = when().get(Endpoints.host + Endpoints.inventory)
                .then().assertThat().statusCode(200).extract().body().as(Map.class);
        Assert.assertTrue(inventory.containsKey("sold"), "Inventory имеет ключ sold");
    }
}
