package Animals.Herbivore;

import Animals.Animal;
import Foods.Food;
import Foods.Grass.Grass;
import Foods.WrongFoodException;

public abstract class Herbivore extends Animal {
    @Override
    public String eat(Food food) throws WrongFoodException {
        if (food instanceof Grass == true) {
            System.out.println(" is eating");
        } else throw new WrongFoodException();


        return null;
    }

}
