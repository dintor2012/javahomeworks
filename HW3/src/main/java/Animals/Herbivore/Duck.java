package Animals.Herbivore;

import Animals.AviarySize;
import Animals.Interfaces.Fly;
import Animals.Interfaces.Run;
import Animals.Interfaces.Swim;
import Animals.Interfaces.Voice;
import Foods.Food;
import Foods.WrongFoodException;

public class Duck extends Herbivore implements Swim, Fly, Voice, Run {
    private String duckVoice = "Quack";

    public Duck(String name, String voice) {
        this.name = name;
        this.duckVoice = voice;
        this.size = AviarySize.Medium;
    }

    public Duck() {
    }

    @Override
    public String eat(Food food) throws WrongFoodException {
        return super.eat(food);
    }


    @Override
    public void Fly() {
        System.out.println("Duck is flying");
    }

    @Override
    public void Run() {
        System.out.println("Duck is running");
    }

    @Override
    public void Swim() {
        System.out.println("Duck is swimming");
    }

    @Override
    public String Voice() {
        return duckVoice;
    }
}
