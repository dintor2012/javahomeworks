package Animals.Herbivore;

import Animals.AviarySize;
import Animals.Interfaces.Fly;
import Animals.Interfaces.Run;
import Animals.Interfaces.Voice;
import Foods.Food;
import Foods.WrongFoodException;

public class Chicken extends Herbivore implements Fly, Voice, Run {

    private String voice = "Cluck";

    public Chicken(String name,String voice){
        this.name = name;
        this.voice = voice;
        this.size = AviarySize.Medium;
    }
    public Chicken(){}



    @Override
    public String eat(Food food) throws WrongFoodException {
        return super.eat(food);
    }

    @Override
    public String Voice() {
        return voice;
    }

    @Override
    public void Fly() {
        System.out.println("Chicken is flying");
    }

    @Override
    public void Run() {
        System.out.println("Chicken is running");
    }
}
