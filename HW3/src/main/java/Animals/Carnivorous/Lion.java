package Animals.Carnivorous;

import Animals.AviarySize;
import Animals.Interfaces.Run;
import Animals.Interfaces.Swim;
import Animals.Interfaces.Voice;
import Foods.Food;
import Foods.WrongFoodException;

public class Lion extends Carnivorous implements Swim, Voice, Run {

    private String voice = "Roarrr";

    public Lion(String name,String voice){
        this.name = name;
        this.voice = voice;
        this.size = AviarySize.Big;
    }

    @Override
    public String eat(Food food) throws WrongFoodException {
        return super.eat(food);
    }

    @Override
    public void Run() {
        System.out.println("Lion is running");
    }

    @Override
    public void Swim() {
        System.out.println("Lion is swimming");
    }

    @Override
    public String Voice() {
        return voice;
    }
}
