package Animals.Carnivorous;
import Animals.AviarySize;
import Animals.Interfaces.Swim;
import Foods.Food;
import Foods.WrongFoodException;

public class Fish extends Carnivorous implements Swim {


    public Fish(String name){
        this.name = name;
        this.size = AviarySize.Small;
    }

    @Override
    public String eat(Food food) throws WrongFoodException {
        return super.eat(food);
    }

    @Override
    public void Swim() {
        System.out.println("Fish is swimming");
    }
}
