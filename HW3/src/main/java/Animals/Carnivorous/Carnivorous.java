package Animals.Carnivorous;
import Animals.Animal;
import Foods.Food;
import Foods.Grass.Grass;
import Foods.WrongFoodException;

public abstract class Carnivorous extends Animal {

    @Override
    public String eat(Food food) throws WrongFoodException {
        if (food instanceof Grass == false) {
            System.out.println(" is eating");
        } else throw new WrongFoodException();

        return null;
    }

}
