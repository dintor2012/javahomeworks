package Animals;

import Foods.Food;
import Foods.WrongFoodException;

public abstract class Animal {
    private String voice;
    protected String name;
    protected AviarySize size;
    public abstract String eat(Food food) throws WrongFoodException;

    public String getName() {
        return name;
    }

    public AviarySize getAviarySize(){
        return size;
    }
    @Override
    public int hashCode(){
        return name.hashCode();
    }
    @Override
    public boolean equals(Object object){
        if(object.getClass() !=this.getClass()) {
            return false;
        }
        else return (object.hashCode() == this.hashCode());
    }

}
