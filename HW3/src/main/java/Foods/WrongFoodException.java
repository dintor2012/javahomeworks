package Foods;

public class WrongFoodException extends Exception {
    public WrongFoodException(){
        super("Incorrect type of food");
    }
}
