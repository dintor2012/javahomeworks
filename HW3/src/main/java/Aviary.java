import Animals.*;

import java.util.HashSet;

public class Aviary<T extends Animal> {
private HashSet<T> animalSet = new HashSet<>();
private AviarySize aviarySize;

public Aviary(AviarySize size){
   this.aviarySize = size;
}

public void addAnimal(T animal){
   switch (animal.getAviarySize()){
      case Small:
         animalSet.add(animal);
         System.out.println("Added animal:" + animal.getName());
         break;

      case Medium:
         if (aviarySize != AviarySize.Small){
            animalSet.add(animal);
            System.out.println("Added animal:" + animal.getName());
         }
         else{
            System.out.println("Cant add this animal. \nAnimal size: " + animal.getAviarySize() + "\nAviary size: " + aviarySize);
         }
         break;


      case Big:
         if(aviarySize != AviarySize.Small && aviarySize != AviarySize.Medium){
            animalSet.add(animal);
            System.out.println("Added animal:" + animal.getName());
         }
            else{
               System.out.println("Cant add this animal. \nAnimal size: " + animal.getAviarySize() + "\nAviary size: " + aviarySize);
         }
         break;


      case Huge:
         if(aviarySize == AviarySize.Huge){
            animalSet.add(animal);
            System.out.println("Added animal:" + animal.getName());
            break;
         }
   }

}
public void deleteAnimal(String name){
   animalSet.remove(name);
}

public Animal showAnimal(String name){
   for(Animal animal:animalSet){
      if(name.hashCode()==animal.getName().hashCode()){
         System.out.println(animal.getName());
         return animal;
      }
      else{
         System.out.println("No animal with this name");
      }
   }
   return null;
}

}
