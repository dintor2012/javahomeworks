import Animals.AviarySize;
import Animals.Carnivorous.Carnivorous;
import Animals.Carnivorous.Fish;
import Animals.Carnivorous.Lion;
import Animals.Carnivorous.Snake;
import Animals.Herbivore.Chicken;
import Animals.Herbivore.Duck;
import Animals.Herbivore.Elephant;
import Animals.Herbivore.Herbivore;
import Animals.Interfaces.Swim;
import Foods.Food;
import Foods.Grass.Grass;
import Foods.Grass.Leafs;
import Foods.Grass.Seeds;
import Foods.Grass.Watergrass;
import Foods.Meat.ChickenMeat;
import Foods.Meat.FishMeat;
import Foods.Meat.Meat;
import Foods.WrongFoodException;
import Worker.Worker;

public class Zoo {

    public static void main(String[] args) throws WrongFoodException {
        //////////Пища////////////
        Food Grass = new Grass();
        Food Meat = new Meat();
        Food Seeds = new Seeds();
        Leafs leaf1 = new Leafs();
        Food Watergrass = new Watergrass();
        FishMeat Fish = new FishMeat();
        Food ChickenMeat = new ChickenMeat();

        ///////////////////////Животные/////////////////////
        Snake snake1 = new Snake("Vasya", "Shhh");
        Duck duck1 = new Duck();
        Fish fish1 = new Fish("Nemo");
        Chicken chicken1 = new Chicken();
        Elephant elephant1 = new Elephant("Bambini", "Pawoooo");
        Worker worker = new Worker();
        Lion lion1 = new Lion("Lev","roaaaaaar");

        Aviary<Carnivorous> carnivorousAviary = new Aviary<>(AviarySize.Small);

        carnivorousAviary.addAnimal(fish1);
        carnivorousAviary.addAnimal(lion1);

        Aviary<Herbivore> herbivoreAviary = new Aviary<>(AviarySize.Medium);
        herbivoreAviary.addAnimal(elephant1);
        herbivoreAviary.showAnimal("Bambini");





    }
}


