package Worker;

import Animals.Animal;
import Animals.Interfaces.Voice;
import Foods.Food;
import Foods.WrongFoodException;

public class Worker {

    public void feed(Animal animal, Food food) throws WrongFoodException {
        animal.eat(food);
    }

    public String getVoice(Voice animal) {
        return animal.Voice();
    }
}
