package model;

public class Kotik {
    private String catName, catMeow, foodName;
    private int catSatiety = 12, catWeight, food;
    private static int countOfCats = 0;

    public Kotik(String name, int weight, int satiety, String meow) {
        this.catName = name;
        this.catWeight = weight;
        this.catSatiety = satiety;
        this.catMeow = meow;
        countOfCats++;
    }

    public Kotik() {
        countOfCats++;
    }

    public void setKotik(String name, int weight, int satiety, String meow) {
        catName = name;
        catWeight = weight;
        catSatiety = satiety;
        catMeow = meow;

    }

    //////////////Геттеры
    public int getSatiety() {
        return catSatiety;
    }

    public String getName() {
        return catName;
    }

    public static int getCountOfCats() {
        return countOfCats;
    }

    public String getCatMeow() {
        return catMeow;
    }

    public int getCatWeight() {
        return catWeight;
    }

    public boolean play() {
        System.out.println(catName + " is playing ");
        catSatiety -= 5;
        return true;
    }

    public boolean sleep() {
        System.out.println(catName + " is sleeping");
        return true;
    }

    public boolean chaseMouse() {
        catSatiety -= 24;
        System.out.println(catName + "chasing mouse");
        return true;
    }

    public boolean scream() {
        System.out.println(catName + "screaming");
        return true;
    }

    public static boolean compareMeows(String meow1, String meow2) {
        return meow1 == meow2;
    }

    public void eat(int food, String foodName) {
        System.out.println("Fed cat with " + foodName + ", count of food " + food);

    }

    public void eat(int food) {
        catSatiety += food;
        System.out.println("Satiety of cat " + catSatiety + ", amount of food " + food);
    }

    public void eat() {
        this.eat(food, foodName);
        //eat(20, meat)
    }

    public void liveAnotherDay() {
        // for (int a = 1; a <= 24; a++)   Цикл, чтобы каждый раз проходил проверку на голод, перед выполнением нового действия
        if (catSatiety <= 0) {
            System.out.println("Feed me");
        } else {
            for (int a = 1; a <= 24; a++) {
                int randNum = (int) (Math.random() * 4 + 1);
                switch (randNum) {
                    case 1 -> play();
                    case 2 -> sleep();
                    case 3 -> chaseMouse();
                    case 4 -> scream();
                }
            }
        }
    }

}

