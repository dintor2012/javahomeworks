import model.Kotik;

public class Application {

    public static void main(String[] args) {
        Kotik kot1 = new Kotik("James", 12, 26, "meow");
        Kotik kot2 = new Kotik();
        kot2.setKotik("Ryan", 15, 18, "mew");
        kot1.chaseMouse();
        kot1.liveAnotherDay();
        System.out.println(kot1.getName() + " satiety " + kot1.getSatiety());
        System.out.println("Cat name: " + kot1.getName() + ", his weight: " + kot1.getCatWeight());
        System.out.println(Kotik.compareMeows(kot1.getCatMeow(), kot2.getCatMeow()));
        System.out.println("Cats created " + Kotik.getCountOfCats());
        kot1.liveAnotherDay();

    }
}
