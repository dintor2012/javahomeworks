package Animals.Herbivore;

import Animals.Interfaces.Fly;
import Animals.Interfaces.Run;
import Animals.Interfaces.Swim;
import Animals.Interfaces.Voice;
import Foods.Food;

public class Duck extends Herbivore implements Swim, Fly, Voice, Run {
    private String duckName = "Donald", duckVoice = "Quack";

    public Duck(String name, String voice) {
        this.duckName = name;
        this.duckVoice = voice;
    }

    public Duck() {
    }

    public String getDuckName() {
        return duckName;
    }

    @Override
    public String eat(Food food){
        return super.eat(food);
    }


    @Override
    public void Fly() {
        System.out.println("Duck is flying");
    }

    @Override
    public void Run() {
        System.out.println("Duck is running");
    }

    @Override
    public void Swim() {
        System.out.println("Duck is swimming");
    }

    @Override
    public String Voice() {
        return duckVoice;
    }
}
