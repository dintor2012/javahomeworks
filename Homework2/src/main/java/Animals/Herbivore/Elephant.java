package Animals.Herbivore;
import Animals.Interfaces.Run;
import Animals.Interfaces.Swim;
import Animals.Interfaces.Voice;
import Foods.Food;

public class Elephant extends Herbivore implements Swim, Voice,Run {
    private String name, voice = "Pawoo";

    public Elephant(String name,String voice){
        this.name = name;
        this.voice = voice;

    }

    public Elephant(){}

    public String getName() {
        return name;
    }

    @Override
    public String eat(Food food){
        return super.eat(food);
    }

    @Override
    public void Run() {
        System.out.println("Elephant is running");
    }

    @Override
    public void Swim() {
        System.out.println("Elephant is swimming");
    }

    @Override
    public String Voice() {
        return voice;
    }
}
