package Animals.Carnivorous;
import Animals.Animal;
import Foods.Food;
import Foods.Grass.Grass;

public abstract class Carnivorous extends Animal{

    @Override
    public String eat(Food food) {
        if (food instanceof Grass == false) {
            System.out.println(" is eating");
        } else if (food instanceof Grass == true) {
            System.out.println("Can't feed  with this type of food");
        }

        return null;
    }

}
