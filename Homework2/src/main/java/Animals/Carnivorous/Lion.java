package Animals.Carnivorous;

import Animals.Interfaces.Run;
import Animals.Interfaces.Swim;
import Animals.Interfaces.Voice;
import Foods.Food;

public class Lion extends Carnivorous implements Swim, Voice, Run {

    private String name,voice = "Roarrr";

    public Lion(String name,String voice){
        this.name = name;
        this.voice = voice;
    }
    public Lion(){}

    @Override
    public String eat(Food food){
        return super.eat(food);
    }

    @Override
    public void Run() {
        System.out.println("Lion is running");
    }

    @Override
    public void Swim() {
        System.out.println("Lion is swimming");
    }

    @Override
    public String Voice() {
        return voice;
    }
}
