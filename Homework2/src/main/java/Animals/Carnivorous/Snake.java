package Animals.Carnivorous;

import Animals.Interfaces.Run;
import Animals.Interfaces.Swim;
import Animals.Interfaces.Voice;
import Foods.Food;

public class Snake extends Carnivorous implements Swim, Voice, Run {

    private String name, voice = "shhhhhhhhh";

    public Snake(String name,String voice) {
        this.name = name;
        this.voice = voice;
    }
    public Snake(){}

    @Override
    public String eat(Food food) {
        return super.eat(food);
    }

    @Override
    public void Run() {
        System.out.println("Snake is running");
    }

    @Override
    public void Swim() {
        System.out.println("Snake is swimming");
    }
    @Override
    public String Voice() {
        return voice;
    }
}

