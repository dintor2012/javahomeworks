package Animals.Carnivorous;
import Animals.Animal;
import Animals.Carnivorous.Carnivorous;
import Animals.Interfaces.Swim;
import Foods.Food;

public class Fish extends Carnivorous implements Swim {
    private String name;

    public Fish(String name){
        this.name = name;
    }

    public Fish(){}

    @Override
    public String eat(Food food) {
        return super.eat(food);
    }

    @Override
    public void Swim() {
        System.out.println("Fish is swimming");
    }
}
