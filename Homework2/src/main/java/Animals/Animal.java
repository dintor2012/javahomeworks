package Animals;

import Foods.Food;

public abstract class Animal {
    private String voice;
    public abstract String eat(Food food);

}
