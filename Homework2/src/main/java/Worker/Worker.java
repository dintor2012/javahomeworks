package Worker;

import Animals.Animal;
import Animals.Interfaces.Voice;
import Foods.Food;

public class Worker {

    public void feed(Animal animal, Food food) {
        animal.eat(food);
    }

    public String getVoice(Voice animal) {
        return animal.Voice();
    }
}
