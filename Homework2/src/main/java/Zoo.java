import Animals.Animal;
import Animals.Carnivorous.Snake;
import Animals.Herbivore.Chicken;
import Animals.Herbivore.Duck;
import Animals.Herbivore.Elephant;
import Animals.Carnivorous.Fish;
import Animals.Interfaces.Voice;
import Animals.Interfaces.Swim;
import Foods.Food;
import Foods.Grass.Leafs;
import Foods.Grass.Seeds;
import Foods.Grass.Watergrass;
import Foods.Meat.ChickenMeat;
import Foods.Meat.FishMeat;
import Foods.Meat.Meat;
import Foods.Grass.Grass;
import Worker.Worker;

import java.util.ArrayList;
import java.util.List;

public class Zoo {

    public static Swim[] createSea(){
        Swim[] animalSwim = new Swim[3];
        animalSwim[0] = new Snake("Lole", "Shhhhhh");
        animalSwim[1] = new Elephant("Kira","Pawoo");
        animalSwim[2] = new Fish("Nemo");
        return animalSwim;
    }

    public static void main(String[] args) {
        //////////Пища////////////
        Food Grass = new Grass();
        Food Meat = new Meat();
        Food Seeds = new Seeds();
        Leafs leaf1 = new Leafs();
        Food Watergrass = new Watergrass();
        FishMeat Fish = new FishMeat();
        Food ChickenMeat = new ChickenMeat();

        ///////////////////////Животные/////////////////////
        Snake snake1 = new Snake("Vasya", "Shhh");
        Duck duck1 = new Duck();
        Fish fish1 = new Fish("Nemo");
        Chicken chicken1 = new Chicken();
        Elephant elephant1 = new Elephant("Bambini", "Pawoooo");
        Worker worker = new Worker();

        ////////////Методы////////////
        duck1.eat(ChickenMeat);
        snake1.eat(ChickenMeat);
        worker.feed(elephant1, Meat);
        worker.feed(snake1, Seeds);
        worker.feed(fish1, Meat);
        worker.feed(fish1, Seeds);
        System.out.println(worker.getVoice(snake1));
        worker.getVoice(snake1);
        //Немая рыба worker.getVoice(fish1);

        for(Swim a:createSea()){
            a.Swim();
        }


       /* ArrayList<Swim> Sea = new ArrayList<Swim>();
        Sea.add(snake1);
        Sea.add(fish1);
        Sea.add(elephant1);

        for(Swim swim:Sea){
            swim.Swim();

        }*/




    }
}


