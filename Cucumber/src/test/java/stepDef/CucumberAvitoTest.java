package stepDef;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class CucumberAvitoTest {
    WebDriver driver;

    @Before
    public void startingDriver() {
        System.setProperty("webdriver.chrome.driver", "C:/chromedriver/chromedriver.exe");
        driver = new ChromeDriver();

    }

    @Пусть("открыт ресурс авито")
    @Test(priority = 1)
    public void Test1() {
        driver.get("https://www.avito.ru/");

    }

    @ParameterType(".*")
    public Goods category(String category) {
        return Goods.valueOf(category);
    }

    @И("в выпадающем списке категорий выбрана {category}")
    @Test(priority = 2)
    public void Test2(Goods category) {
        Select category1 = new Select(driver.findElement(By.id("category")));
        category1.selectByVisibleText(category.getName());

    }

    @И("в поле поиска введено значение {string}")
    @Test(priority = 3)
    public void Test3(String good) throws InterruptedException {
        WebElement search = driver.findElement(By.className("input-input-Zpzc1"));
        search.sendKeys(good);
        search.sendKeys(Keys.ENTER);

    }

    @И("активирован чекбокс только с фотографией")
    @Test(priority = 4)
    public void Test4() {
        driver.findElement(By.xpath("//input[@name='withImagesOnly']")).sendKeys(Keys.SPACE);

    }

    @Тогда("кликнуть по выпадающему списку региона")
    @Test(priority = 5)
    public void Test5() {
        WebElement citySearch = driver.findElement(By.className("main-text-_Thor"));
        citySearch.click();

    }

    @Тогда("в поле регион введено значение {word}")
    @Test(priority = 6)
    public void Test6(String city) {
        WebElement inputCity = driver.findElement(By.className("suggest-input-rORJM"));
        inputCity.sendKeys(city);
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span/strong")));
        WebElement findFirstValue = driver.findElement(By.xpath("//span/strong"));
        findFirstValue.click();
    }

    @И("нажата кнопка показать объявления")
    @Test(priority = 7)
    public void Test7() {
        WebElement acceptButton = driver.findElement(By.xpath("//button[@data-marker='popup-location/save-button']"));
        acceptButton.click();

    }

    @Тогда("открылась страница результаты по запросу {word}")
    @Test(priority = 8)
    public void Test8(String query) {
        String i = driver.getTitle();
        Assert.assertTrue(i.contains(query), "Not equal");
    }

    @ParameterType(".*")
    public Sort sort(String sortBy) {
        return Sort.valueOf(sortBy);
    }

    @И("в выпадающем списке сортировка выбрано значение {sort}")
    @Test(priority = 9)
    public void Test9(Sort sortName) {
        Select selectSort = new Select(driver.findElement(By.xpath("//div[@class='select-select-box-jJiQW select-size-s-VX5kS']/select")));
        selectSort.selectByVisibleText(sortName.getText());
    }

    @И("в консоль выведено значение названия и цены {word} первых товаров")
    @Test(priority = 10)
    public void Test10(String numberOfGoods) {
        List<WebElement> elem = driver.findElements(By.xpath("//div[@data-marker='catalog-serp']/div[position() <= " + numberOfGoods + "]"));
        for (WebElement element : elem) {
            System.out.println(element.findElement(By.xpath(".//h3[@itemprop='name']")).getText());
            System.out.println(element.findElement(By.xpath(".//meta[@itemprop='price']")).getAttribute("content"));
        }
    }

    @After
    public void closeDriver() {
        driver.quit();
    }

}



