package stepDef;

@SuppressWarnings("ALL")
public enum Sort {
    По_умолчанию(0, "По умолчанию"),
    Дешевле(1, "Дешевле"),
    Дороже(2, "Дороже"),
    По_дате(3, "По дате");

    private int id;
    private String text;

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    Sort(int id, String text) {
        this.id = id;
        this.text = text;
    }
}
