import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        String firstNum;
        String secondNum;
        String op;
        Calculator calculator = new Calculator();

        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("\nВведите цифру с операцией, " +
                    "Для выхода введите '0':\n" +
                    "1 - сложение\n" +
                    "2 - вычитание\n" +
                    "3 - умножение\n" +
                    "4 - деление\n");

            op = scanner.nextLine();
            if (op.equals("0")) {
                break;
            }

            System.out.println("Введите первое число:");
            firstNum = scanner.nextLine();
            System.out.println("Введите второе число:");
            secondNum = scanner.nextLine();

            System.out.print("Ответ: ");
            System.out.println(calculator.calculation(op, firstNum, secondNum));

        }
    }

}
