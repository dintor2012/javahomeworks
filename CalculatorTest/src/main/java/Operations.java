public interface Operations {

    int addition(int first, int second);

    int subtraction(int first, int second);

    int multiply(int first, int second);

    double division(int first, int second) throws ArithmeticException;

    Number calculation(String op, String firstNumStr, String secondNumStr);

}
