public class Calculator implements Operations {

    public int addition(int first, int second) {
        return first + second;
    }

    public int subtraction(int first, int second) {
        return first - second;
    }

    public int multiply(int first, int second) {
        return first * second;
    }

    public double division(int first, int second) throws ArithmeticException {
        if (second == 0) {
            throw new ArithmeticException("Деление на ноль невозможно.");
        }
        return (double) first / (double) second;
    }

    public Number calculation(String op, String firstNumStr, String secondNumStr) {
        int firstNum = Integer.parseInt(firstNumStr);
        int secondNum = Integer.parseInt(secondNumStr);
        switch (op) {
            case "1":
                return addition(firstNum, secondNum);
            case "2":
                return subtraction(firstNum, secondNum);
            case "3":
                return multiply(firstNum, secondNum);
            case "4":
                return division(firstNum, secondNum);
            default: {
                throw new NumberFormatException("Неверное число! Должно быть указано целое число от 1 до 4.");

            }
        }
    }

}
