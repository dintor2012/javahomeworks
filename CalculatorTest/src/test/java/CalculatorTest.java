import org.testng.Assert;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorTest {

   /* @BeforeTest
    public void setUp(){
        Calculator calculator = new Calculator();
    }        */
    Calculator calculator = new Calculator();

    @DataProvider(name = "rightDataForCalculationTest")
    public Object[][] rightDataForCalculationTest() {
        return new Object[][]{
                {2147, "1", "2146", "1"},
                {0, "1", "-1", "1"},
                {0, "2", "1", "1"},
                {-4, "2", "-3", "1"},
                {1, "3", "1", "1"},
                {0, "3", "3", "0"},
                {1.0, "4", "1", "1"},
                {-25.0, "4", "50", "-2"}

        };
    }

    @Test(groups = "Right test",testName ="Test with right data(Positive)", dataProvider = "rightDataForCalculationTest")
    public void calculationTest(Number expectedResult, String op, String firstNum, String secondNum) {
        Assert.assertEquals(calculator.calculation(op, firstNum, secondNum), expectedResult, "Неверный результат вычисления");
    }

    @Test(testName ="Division by zero", expectedExceptions = ArithmeticException.class)
    public void zeroDivisionTest() {
        calculator.division(1, 0);
    }

    @DataProvider(name = "wrongDataForArgumentTest")
    public Object[][] wrongDataForArgumentTest() {
        return new Object[][]{
                {"1", null, "1"},
                {"2", "abc", "1"},
                {"5", "123", "1"},
                {"0", "-23", "1"},
                {null,null,null }
        };
    }

    @Test(testName = "Test with wrong data(Negative)", dataProvider = "wrongDataForArgumentTest", expectedExceptions = NumberFormatException.class)
    public void wrongArgumentTest(String op, String firstNum, String secondNum) {
        calculator.calculation(op, firstNum, secondNum);

    }

}
